package uz.pdp.model.user;

import uz.pdp.model.enums.CustomerRole;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class User {

    private Integer id;
    private String name;
    private String password;
    private CustomerRole role = CustomerRole.CUSTOMER_ROLE;
    private double balance = 35000;
    private int loginAttempt = 0;
    private LocalDateTime created_at;
    private LocalDateTime updated_at;
    private LocalDateTime last_online;

    public User(Integer id, String name, String password, CustomerRole role, double balance, LocalDateTime created_at, LocalDateTime updated_at, LocalDateTime last_online) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.balance = balance;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.last_online = last_online;
    }

    public User(Integer id, String name, String password, CustomerRole role, double balance, LocalDateTime created_at, LocalDateTime updated_at) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.balance = balance;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public User(Integer id, String name, String password, LocalDateTime created_at) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.created_at = created_at;
    }

    public User(Integer id, String name, String password, LocalDateTime created_at, LocalDateTime updated_at) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getLastOnlineFormat() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return last_online.format(format);
    }

    public LocalDateTime getLast_online() {
        return last_online;
    }

    public void setLast_online(LocalDateTime last_online) {
        this.last_online = last_online;
    }

    public int getLoginAttempt() {
        return loginAttempt;
    }

    public void setLoginAttempt(int loginAttempt) {
        this.loginAttempt = loginAttempt;
    }

    public String getDateFormat() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return created_at.format(format);
    }

    public String getUpdateFormat() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return updated_at.format(format);
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public LocalDateTime getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(LocalDateTime updated_at) {
        this.updated_at = updated_at;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public User(Integer id, String name, String password, CustomerRole role, double balance) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
        this.balance = balance;
    }

    public User() {
    }

    public User(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public User(Integer id, String name, String password, CustomerRole role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.role = role;
    }

    public User(Integer id, String name, String password) {
        this.id = id;
        this.name = name;
        this.password = password;
    }

    public CustomerRole getRole() {
        return role;
    }

    public void setRole(CustomerRole role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                '}';
    }
}
