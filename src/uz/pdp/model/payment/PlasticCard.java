package uz.pdp.model.payment;

import uz.pdp.model.user.User;

public class PlasticCard {
    private Integer id;
    private Long cardNumber;
    private String pinCode;
    private User user;
    private double balance;
    private int pinCodeAttempt = 0;

    public PlasticCard() {
    }

    public PlasticCard(Integer id, Long cardNumber, String pinCode, User user, double balance, int pinCodeAttempt) {
        this.id = id;
        this.cardNumber = cardNumber;
        this.pinCode = pinCode;
        this.user = user;
        this.balance = balance;
        this.pinCodeAttempt = pinCodeAttempt;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(Long cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public int getPinCodeAttempt() {
        return pinCodeAttempt;
    }

    public void setPinCodeAttempt(int pinCodeAttempt) {
        this.pinCodeAttempt = pinCodeAttempt;
    }

    @Override
    public String toString() {
        return "Card{" +
                "id=" + id +
                ", cardNumber=" + cardNumber +
                ", pinCode='" + pinCode + '\'' +
                ", user=" + user +
                ", balance=" + balance +
                ", pinCodeAttempt=" + pinCodeAttempt +
                '}';
    }
}
