package uz.pdp.model.payment;

public class PayType {
    private Integer id;
    private String name;
    private double commissonFee;

    public PayType() {
    }

    public PayType(Integer id, String name, double commissonFee) {
        this.id = id;
        this.name = name;
        this.commissonFee = commissonFee;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCommissonFee() {
        return commissonFee;
    }

    public void setCommissonFee(double commissonFee) {
        this.commissonFee = commissonFee;
    }

    @Override
    public String toString() {
        return "PayType{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", commissonFee=" + commissonFee +
                '}';
    }
}
