package uz.pdp.model.payment;

import java.util.List;

public class Bank {
    private Integer id;
    private String bankName;
    private List<PlasticCard> plasticCards;

    public Bank() {
    }

    public Bank(Integer id, String bankName, List<PlasticCard> plasticCards) {
        this.id = id;
        this.bankName = bankName;
        this.plasticCards.addAll(plasticCards);
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public List<PlasticCard> getCards() {
        return plasticCards;
    }

    public void setCards(List<PlasticCard> plasticCards) {
        this.plasticCards = plasticCards;
    }

    @Override
    public String toString() {
        return "Bank{" +
                "id=" + id +
                ", bankName='" + bankName + '\'' +
                ", plasticCards=" + plasticCards +
                '}';
    }
}
