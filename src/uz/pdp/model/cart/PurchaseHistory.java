package uz.pdp.model.cart;

import uz.pdp.model.payment.PayType;
import uz.pdp.model.product.Cloth;
import uz.pdp.model.user.User;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class PurchaseHistory {

    private Integer id;
    private User user;
    private List<Cloth> cloths = new ArrayList<>();
    private PayType payType;
    private LocalDateTime created_at;

    public String getDateFormat() {
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        return created_at.format(format);
    }

    public PurchaseHistory(Integer id, User user, List<Cloth> cloths, PayType payType, LocalDateTime created_at) {
        this.id = id;
        this.user = user;
        this.cloths.addAll(cloths);
        this.payType = payType;
        this.created_at = created_at;
    }

    public LocalDateTime getCreated_at() {
        return created_at;
    }

    public void setCreated_at(LocalDateTime created_at) {
        this.created_at = created_at;
    }

    public PurchaseHistory() {
    }

    public PurchaseHistory(Integer id, User user, List<Cloth> cloths, PayType payType) {
        this.id = id;
        this.user = user;
        this.cloths.addAll(cloths);
        this.payType = payType;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Cloth> getCloths() {
        return cloths;
    }

    public void setCloths(List<Cloth> cloths) {
        this.cloths.addAll(cloths);
    }

    public PayType getPayType() {
        return payType;
    }

    public void setPayType(PayType payType) {
        this.payType = payType;
    }

    @Override
    public String toString() {
        return "PurchaseHistory{" +
                "id=" + id +
                ", user=" + user +
                ", cloths=" + cloths +
                ", payType=" + payType +
                '}';
    }
}
