package uz.pdp.model.cart;

import uz.pdp.model.product.Cloth;
import uz.pdp.model.user.User;

public class Cart {

    private Integer id;
    private User user;
    private Cloth cloth;

    public Cart() {
    }

    public Cart(Integer id, User user, Cloth cloth) {
        this.id = id;
        this.user = user;
        this.cloth = cloth;
    }

    public Cart(Integer id, Cloth cloth) {
        this.id = id;
        this.cloth = cloth;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getCustomer() {
        return user;
    }

    public void setCustomer(User user) {
        this.user = user;
    }

    public Cloth getClothe() {
        return cloth;
    }

    public void setClothe(Cloth cloth) {
        this.cloth = cloth;
    }

    @Override
    public String toString() {
        return "Cart{" +
                "id=" + id +
                ", customer=" + user +
                ", clothe=" + cloth +
                '}';
    }
}
