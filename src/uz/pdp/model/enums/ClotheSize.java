package uz.pdp.model.enums;

public enum ClotheSize {
    L,
    XL,
    XX,
    S,
    M
}
