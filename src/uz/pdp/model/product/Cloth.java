package uz.pdp.model.product;

import uz.pdp.model.enums.ClotheColor;
import uz.pdp.model.enums.ClotheSize;

public class Cloth {

    private Integer id;
    private String name;
    private ClotheSize size;
    private ClotheColor color;
    private double price;
    private double quantity;
    private double discount;

    public Cloth() {
    }

    public Cloth(Integer id, String name, ClotheSize size, ClotheColor color, double price, double quantity, double discount) {
        this.id = id;
        this.name = name;
        this.size = size;
        this.color = color;
        this.price = price;
        this.quantity = quantity;
        this.discount = discount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ClotheSize getSize() {
        return size;
    }

    public void setSize(ClotheSize size) {
        this.size = size;
    }

    public ClotheColor getColor() {
        return color;
    }

    public void setColor(ClotheColor color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    @Override
    public String toString() {
        return "Clothe{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", color=" + color +
                ", price=" + price +
                ", quantity=" + quantity +
                ", discount=" + discount +
                '}';
    }
}
