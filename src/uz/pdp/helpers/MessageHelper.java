package uz.pdp.helpers;

public interface MessageHelper {
    String WRONG_OPTION = "Wrong Option";
    String CLOTHE_NOT_FOUND = "Clothe not found";
    String DELETED = "Deleted";
    String CREATED = "Created";
    String UPDATED = "Updated";
    String INVALID_QTY = "Invalid quantity";
    String INVALID_DISCOUNT = "Invalid Discount";
    String INVALID_PRICE = "Invalid Price";
    String INVALID_SIZE = "Invalid size";
    String INVALID_COLOR = "Invalid color";
    String SIZE_EXIST = "Size already exist in this cloth";
    String COLOR_EXIST = "Color already exist in this cloth";
    String ALREADY_EXIST = "Already exist";
    String INVALID_AMOUNT = "Invalid amount";
    String PAY_NOT_FOUND = "PayType not found";
    String SUCCESS = "Success";
    String REMOVED = "Removed";
    String BUY = "Bought";
    String CHECK_BALANCE = "Check your balance";
    String BLOCK_LIST = "Block list";
    String NOT_FOUND = "Not found";
    String NO_MONEY = "No money";
}
