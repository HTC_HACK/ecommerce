package uz.pdp.helpers.exceptions;

import static uz.pdp.DataBase.TEXT_RED;
import static uz.pdp.helpers.MessageHelper.NO_MONEY;

public class NoMoney extends RuntimeException {

    public NoMoney(String errorMessage) {
        super(errorMessage);
    }

    public NoMoney() {
        super(TEXT_RED + NO_MONEY + TEXT_RED);
    }

}
