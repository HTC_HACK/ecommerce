package uz.pdp;

import uz.pdp.model.cart.Cart;
import uz.pdp.model.cart.PurchaseHistory;
import uz.pdp.model.enums.ClotheColor;
import uz.pdp.model.enums.ClotheSize;
import uz.pdp.model.enums.CustomerRole;
import uz.pdp.model.payment.PayType;
import uz.pdp.model.product.Cloth;
import uz.pdp.model.user.User;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class DataBase {

    public static final String TEXT_RESET = "\u001B[0m";
    public static final String TEXT_BLACK = "\u001B[30m";
    public static final String TEXT_RED = "\u001B[31m";
    public static final String TEXT_GREEN = "\u001B[32m";
    public static final String TEXT_YELLOW = "\u001B[33m";
    public static final String TEXT_BLUE = "\u001B[34m";
    public static final String TEXT_PURPLE = "\u001B[35m";
    public static final String TEXT_CYAN = "\u001B[36m";
    public static final String TEXT_WHITE = "\u001B[37m";

    public static Scanner scannerInt = new Scanner(System.in);
    public static Scanner scannerStr = new Scanner(System.in);
    public static Scanner scannerDou = new Scanner(System.in);
    public static List<Cloth> cloths = new ArrayList<>(
            Arrays.asList(
                    new Cloth(1, "Shirt", ClotheSize.XX, ClotheColor.WHITE, 5000, 50, 0),
                    new Cloth(2, "T-Shirt", ClotheSize.L, ClotheColor.BLUE, 8000, 20, 0)
            )
    );

    public static LocalDateTime created_at = LocalDateTime.now();
    public static LocalDateTime updated_at = LocalDateTime.now();

    public static List<User> users = new ArrayList<>(
            Arrays.asList(
                    new User(1, "admin", "1111", CustomerRole.ADMIN, 50000, created_at, updated_at, created_at)
            )
    );
    public static List<Cart> carts = new ArrayList<>();
    public static List<Cart> myCarts = new ArrayList<>();
    public static List<PurchaseHistory> customerHistories = new ArrayList<>();
    public static List<PayType> payTypes = new ArrayList<>(
            Arrays.asList(
                    new PayType(1, "CLICK", 0.5),
                    new PayType(2, "PAYME", 0.6)
            )
    );
    public static List<Cloth> customerCloths = new ArrayList<>();


}
