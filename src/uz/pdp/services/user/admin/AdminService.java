package uz.pdp.services.user.admin;

import uz.pdp.model.user.User;
import uz.pdp.services.user.admin.action.ClothServiceImpl;
import uz.pdp.services.user.admin.action.CustomerSettingsImpl;
import uz.pdp.services.user.admin.action.PaymentServiceImpl;
import uz.pdp.services.auth.AuthServiceImpl;
import uz.pdp.services.history.HistoryServiceImpl;

import static uz.pdp.DataBase.TEXT_CYAN;
import static uz.pdp.DataBase.scannerInt;

public class AdminService implements uz.pdp.services.interfaces.user.AdminService {
    static ClothServiceImpl clotheInterface = new ClothServiceImpl();
    static HistoryServiceImpl historyServiceImpl = new HistoryServiceImpl();
    static AuthServiceImpl authInterface = new AuthServiceImpl();
    static PaymentServiceImpl paymentService = new PaymentServiceImpl();
    static CustomerSettingsImpl customerSettings = new CustomerSettingsImpl();

    @Override
    public void adminMenu(User user) {
        while (true) {
            System.out.println(TEXT_CYAN + "1=>Clothes Action. 2=>PayAction. 3=>CustomerAction. 4=>HistoryAction. 5.Balance." + " 0=>Logout" + TEXT_CYAN);
            int option = scannerInt.nextInt();
            switch (option) {
                case 1:
                    clotheInterface.crudMenu();
                    break;
                case 2:
                    paymentService.crudMenu();
                    break;
                case 3:
                    customerSettings.settingsMenu();
                    break;
                case 4:
                    historyServiceImpl.historyMenu(user);
                    break;
                case 5:
                    System.out.println("Balance : " + user.getBalance());
                    break;
                case 0:
                    return;
            }
        }
    }


}
