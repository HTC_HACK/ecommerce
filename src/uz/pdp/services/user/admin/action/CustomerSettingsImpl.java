package uz.pdp.services.user.admin.action;

import uz.pdp.model.user.User;
import uz.pdp.services.action.FindObject;
import uz.pdp.services.interfaces.user.CustomerSettings;

import java.time.LocalDateTime;
import java.util.InputMismatchException;
import java.util.Scanner;

import static uz.pdp.DataBase.*;
import static uz.pdp.helpers.MessageHelper.*;

public class CustomerSettingsImpl implements CustomerSettings {

    static FindObject findObject = new FindObject();

    @Override
    public void list() {
        users.stream().forEach(s ->
                System.out.println(TEXT_GREEN + "name : " + s.getName() + ", role : " + s.getRole() + ", created : " + s.getDateFormat() + ", updated : " + s.getUpdateFormat()
                        + " , lastOnline : " + s.getLastOnlineFormat() + TEXT_GREEN));
    }

    @Override
    public void blockList() {
        users.stream()
                .filter(user -> user.getLoginAttempt() >= 3)
                .forEach(s ->
                        System.out.println(TEXT_GREEN + "name : " + s.getName() + ", role : " + s.getRole() + ", created : " + s.getDateFormat() + ", updated : " + s.getUpdateFormat() + TEXT_GREEN));

    }

    @Override
    public void unblock() {
        blockList();
        System.out.println("Enter user id");
        try {
            int userId = new Scanner(System.in).nextInt();
            User user = users.stream().filter(user1 -> user1.getLoginAttempt() >= 3 && user1.getId() == userId)
                    .findFirst().orElse(null);
            if (user != null) {
                user.setLoginAttempt(0);
                LocalDateTime updated_at = LocalDateTime.now();
                user.setUpdated_at(updated_at);
                System.out.println(SUCCESS);
            } else {
                System.out.println(NOT_FOUND);
                return;
            }
        } catch (InputMismatchException e) {
            System.out.println(NOT_FOUND);
            return;
        }
    }

    @Override
    public void settingsMenu() {
        while (true) {
            System.out.println(TEXT_YELLOW + "1=>Customer List. 2=>Block List. 3=>Unblock. 0=>Back" + TEXT_YELLOW);
            try {
                int option = new Scanner(System.in).nextInt();
                switch (option) {
                    case 1:
                        list();
                        break;
                    case 2:
                        blockList();
                        break;
                    case 3:
                        unblock();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println(WRONG_OPTION);
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_OPTION);
            }

        }
    }
}
