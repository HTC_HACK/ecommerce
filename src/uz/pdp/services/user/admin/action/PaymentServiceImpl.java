package uz.pdp.services.user.admin.action;

import uz.pdp.model.payment.PayType;
import uz.pdp.services.action.FindObject;
import uz.pdp.services.interfaces.crud.CrudRepo;
import uz.pdp.services.interfaces.builder.PayTypeBuilder;

import java.util.InputMismatchException;
import java.util.Scanner;

import static uz.pdp.DataBase.*;
import static uz.pdp.helpers.MessageHelper.*;

public class PaymentServiceImpl implements CrudRepo {

    static FindObject findObject = new FindObject();


    @Override
    public void create() {
        System.out.println("Enter paytype");
        String name = new Scanner(System.in).nextLine();
        PayType payType = findObject.returnPayType(name);
        if (payType != null) {
            System.out.println(ALREADY_EXIST);
            return;
        }
        System.out.println("Enter commissinFee");
        try {
            double fee = new Scanner(System.in).nextDouble();
            PayTypeBuilder payTypeBuilder = PayType::new;
            PayType payTypeBuild = payTypeBuilder.build((int) (Math.random() * 10000), name, fee);
            payTypes.add(payTypeBuild);
            System.out.println(TEXT_BLUE + CREATED + TEXT_BLUE);
        } catch (InputMismatchException e) {
            System.out.println(INVALID_AMOUNT);
            return;
        }

    }

    @Override
    public void read() {
        System.out.println(TEXT_GREEN + "-------------------------------");
        payTypes.stream()
                .forEach(s -> {
                    System.out.println("id : " + s.getId() + ", name : " +
                            s.getName() + ", fee : " + s.getCommissonFee());
                    System.out.println("-------------------------------" + TEXT_GREEN);
                });
    }

    @Override
    public void update() {
        read();
        System.out.println("Enter id for update");
        try {
            int id = new Scanner(System.in).nextInt();
            PayType payType = findObject.returnPayType(id);
            System.out.println(payType.getId());
            System.out.println("Enter commissonFee");
            double fee = new Scanner(System.in).nextDouble();
            payType.setCommissonFee(fee);
            System.out.println(TEXT_GREEN + UPDATED + TEXT_GREEN);
        } catch (InputMismatchException e) {
            System.out.println(TEXT_RED + PAY_NOT_FOUND + TEXT_RED);
        }
    }

    @Override
    public void delete() {
        read();
        System.out.println("Enter id for delete");
        try {
            int id = new Scanner(System.in).nextInt();
            PayType payType = findObject.returnPayType(id);
            System.out.println(payType.getId());
            payTypes.remove(payType);
            System.out.println(TEXT_RED + DELETED + TEXT_RED);
        } catch (InputMismatchException e) {
            System.out.println(TEXT_RED + PAY_NOT_FOUND + TEXT_RED);
        }

    }

    @Override
    public void crudMenu() {
        while (true) {
            System.out.println(TEXT_CYAN + "1=>PayType List. 2=>Add PayType. 3=>Edit PayType. 4=>DeletePayType. 0=>Back" + TEXT_CYAN);
            try {
                int optionPay = new Scanner(System.in).nextInt();
                switch (optionPay) {
                    case 1:
                        read();
                        break;
                    case 2:
                        create();
                        break;
                    case 3:
                        update();
                        break;
                    case 4:
                        delete();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println(WRONG_OPTION);
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_OPTION);
            }
        }
    }


    @Override
    public String returnName() {
        return null;
    }


}
