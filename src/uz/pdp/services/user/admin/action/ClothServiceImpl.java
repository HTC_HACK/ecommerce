package uz.pdp.services.user.admin.action;

import uz.pdp.model.enums.ClotheColor;
import uz.pdp.model.enums.ClotheSize;
import uz.pdp.model.product.Cloth;
import uz.pdp.services.action.FindObject;
import uz.pdp.services.interfaces.builder.ClotheBuilder;
import uz.pdp.services.interfaces.crud.CrudRepo;

import java.util.InputMismatchException;
import java.util.Scanner;

import static uz.pdp.DataBase.*;
import static uz.pdp.helpers.MessageHelper.*;
import static uz.pdp.model.enums.ClotheColor.*;
import static uz.pdp.model.enums.ClotheSize.*;

public class ClothServiceImpl implements CrudRepo {

    static FindObject findObject = new FindObject();

    @Override
    public void create() {
        System.out.println("Enter cloth name");
        String name = new Scanner(System.in).nextLine();
        Cloth clothName = cloths.stream().filter(s -> s.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
        System.out.println("Choose a size");
        System.out.println("1=>L. 2=>XL. 3=>XX. 4=>S. 5=>M");
        try {
            int option = new Scanner(System.in).nextInt();
            System.out.println(option);
            ClotheSize size = null;
            switch (option) {
                case 1:
                    size = L;
                    break;
                case 2:
                    size = XL;
                    break;
                case 3:
                    size = XX;
                    break;
                case 4:
                    size = S;
                    break;
                case 5:
                    size = M;
                    break;
                default:
                    System.out.println(WRONG_OPTION);
                    break;

            }
            try {
                size.name();
                try {
                    System.out.println("Enter price");
                    double price = new Scanner(System.in).nextDouble();
                    try {
                        System.out.println("Choose color");
                        System.out.println("1=>Red. 2=>Black. 3=>White. 4=>Blue");
                        int colorOption = new Scanner(System.in).nextInt();
                        ClotheColor color = null;
                        switch (colorOption) {
                            case 1:
                                color = RED;
                                break;
                            case 2:
                                color = BLACK;
                                break;
                            case 3:
                                color = WHITE;
                                break;
                            case 4:
                                color = BLUE;
                                break;
                        }
                        System.out.println("Enter quantity");
                        try {
                            if (clothName != null && clothName.getSize().equals(size) && clothName.getColor().equals(color)) {
                                System.out.println(TEXT_RED + COLOR_EXIST);
                                return;
                            }
                            double quantity = new Scanner(System.in).nextDouble();
                            System.out.println("Enter discount");
                            try {
                                double discount = new Scanner(System.in).nextDouble();
                                ClotheBuilder clotheBuilder = Cloth::new;
                                Cloth cloth = clotheBuilder.build((int) (Math.random() * 10000), name,
                                        size, color, price, quantity, discount);
                                cloths.add(cloth);

                                System.out.println(TEXT_BLUE + CREATED + TEXT_BLUE);
                            } catch (InputMismatchException e) {
                                System.out.println(INVALID_DISCOUNT);
                                return;
                            }
                        } catch (InputMismatchException e) {
                            System.out.println(INVALID_QTY);
                            return;
                        }
                    } catch (NullPointerException e) {
                        System.out.println(INVALID_PRICE);
                        return;
                    }

                } catch (InputMismatchException e) {
                    System.out.println(WRONG_OPTION);
                    return;
                }
            } catch (NullPointerException e) {
                System.out.println(WRONG_OPTION);
                return;
            }
        } catch (InputMismatchException e) {
            System.out.println(WRONG_OPTION);
            return;
        }
    }

    @Override
    public void read() {
        System.out.println(TEXT_CYAN + "----------------------------------------------------");
        cloths.stream().forEach(s -> {
            System.out.println("| id : " + s.getId() + ", name : " + s.getName() + ", quantity : " + s.getQuantity() +
                    ", size : " + s.getSize() + TEXT_RED + ",old price : " + s.getPrice() + TEXT_RED + TEXT_CYAN + ", new price : " + TEXT_CYAN + (s.getPrice() - s.getPrice() * s.getDiscount() / 100) + ", color : " + s.getColor());
            System.out.println("----------------------------------------------------" + TEXT_CYAN);
        });
    }

    @Override
    public void update() {
        read();
        System.out.println("Enter id for update");
        int clotheID = scannerInt.nextInt();
        try {
            Cloth cloth = findObject.returnClothe(clotheID);
            String clotheName = returnName();
            System.out.println(TEXT_CYAN + "-----------------------------------------");
            cloths.stream().filter(s -> s.getName().equalsIgnoreCase(clotheName))
                    .forEach(s -> {
                        System.out.println("id : " + s.getId() + ", name : " + s.getName()
                                + ", size : " + s.getSize() + ", color : " + s.getColor());
                        System.out.println(TEXT_CYAN + "-----------------------------------------");
                    });
            System.out.println("Enter size");
            try {
                String size = new Scanner(System.in).nextLine();
                ClotheSize sizeCloth = ClotheSize.valueOf(size);
                System.out.println(TEXT_CYAN + "-----------------------------------------");
                cloths.stream().filter(s -> s.getName().equalsIgnoreCase(clotheName) && s.getSize().equals(sizeCloth))
                        .forEach(s -> {
                            System.out.println("id : " + s.getId() + ", name : " + s.getName()
                                    + ", size : " + s.getSize() + ", color : " + s.getColor());
                            System.out.println(TEXT_CYAN + "-----------------------------------------");
                        });
                System.out.println("Enter color");
                try {
                    String color = new Scanner(System.in).nextLine();
                    ClotheColor colorCloth = ClotheColor.valueOf(color);
                    System.out.println(TEXT_CYAN + "-----------------------------------------");
                    cloths.stream().filter(s ->
                                    s.getName().equalsIgnoreCase(clotheName) &&
                                            s.getSize().equals(sizeCloth) && s.getColor().equals(colorCloth))
                            .forEach(s -> {
                                System.out.println("id : " + s.getId() + ", name : " + s.getName()
                                        + ", size : " + s.getSize() + ", color : " + s.getColor());
                                System.out.println("-----------------------------------------" + TEXT_CYAN);
                            });
                    System.out.println("Enter quantity");
                    try {
                        double quantity = new Scanner(System.in).nextDouble();
                        Cloth cloth1 = cloths.stream().filter(s ->
                                        s.getName().equalsIgnoreCase(clotheName) &&
                                                s.getSize().equals(sizeCloth) && s.getColor().equals(colorCloth)).findFirst()
                                .orElse(null);
                        System.out.println("Enter price");
                        try {
                            double price = new Scanner(System.in).nextDouble();
                            cloth1.setPrice(price);
                            cloth1.setQuantity(cloth1.getQuantity() + quantity);
                            System.out.println(TEXT_GREEN + UPDATED + TEXT_GREEN);
                        } catch (InputMismatchException e) {
                            System.out.println(INVALID_PRICE);
                            return;
                        }
                    } catch (InputMismatchException e) {
                        System.out.println(INVALID_QTY);
                        return;
                    }
                } catch (IllegalArgumentException e) {
                    System.out.println(INVALID_COLOR);
                    return;
                }
            } catch (IllegalArgumentException e) {
                System.out.println(INVALID_SIZE);
                return;
            }

        } catch (NullPointerException e) {
            System.out.println(CLOTHE_NOT_FOUND);
            return;
        }
    }

    @Override
    public void delete() {
        read();
        System.out.println("Enter id for delete");
        int clotheID = scannerInt.nextInt();
        try {
            Cloth cloth = findObject.returnClothe(clotheID);
            System.out.println(cloth.getName());
            cloths.remove(cloth);
            System.out.println(TEXT_RED + DELETED + TEXT_RED);
        } catch (NullPointerException e) {
            System.out.println(CLOTHE_NOT_FOUND);
        }

    }

    @Override
    public void crudMenu() {
        while (true) {
            System.out.println(TEXT_YELLOW + "1=>Clothes List. 2=>Add Clothes. 3=>EditClothes. 4=>DeleteClothes. 0=>Back" + TEXT_YELLOW);
            try {
                int option = new Scanner(System.in).nextInt();
                switch (option) {
                    case 1:
                        read();
                        break;
                    case 2:
                        create();
                        break;
                    case 3:
                        update();
                        break;
                    case 4:
                        delete();
                        break;
                    case 0:
                        return;
                    default:
                        System.out.println(WRONG_OPTION);
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_OPTION);
            }

        }
    }


    @Override
    public String returnName() {
        read();
        System.out.println("Enter clothe name for update");
        String clotheName = scannerInt.nextLine();
        return clotheName;
    }
}
