package uz.pdp.services.user.customer.cart;

import uz.pdp.model.cart.Cart;
import uz.pdp.model.cart.PurchaseHistory;
import uz.pdp.model.enums.CustomerRole;
import uz.pdp.model.payment.PayType;
import uz.pdp.model.product.Cloth;
import uz.pdp.model.user.User;
import uz.pdp.services.action.FindObject;
import uz.pdp.services.auth.AuthServiceImpl;
import uz.pdp.services.history.HistoryServiceImpl;
import uz.pdp.services.interfaces.cart.CartService;
import uz.pdp.services.user.admin.action.ClothServiceImpl;
import uz.pdp.services.user.admin.action.PaymentServiceImpl;
import uz.pdp.services.user.customer.CustomerServiceImpl;
import uz.pdp.templates.Ecommerce;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import static uz.pdp.DataBase.*;
import static uz.pdp.helpers.MessageHelper.*;

public class CartServiceImpl implements CartService {
    static ClothServiceImpl clotheService = new ClothServiceImpl();
    static AuthServiceImpl customerSerivce = new AuthServiceImpl();
    static CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl();
    static HistoryServiceImpl historyService = new HistoryServiceImpl();
    static PaymentServiceImpl paymentService = new PaymentServiceImpl();
    static FindObject findObject = new FindObject();
    static Ecommerce ecommerce = new Ecommerce();

    @Override
    public void cartMenu(User user) {
        user.setLoginAttempt(0);
        AtomicBoolean flag = new AtomicBoolean(false);

        carts.stream().forEach(s -> {
            Cart cart = new Cart((int) (Math.random() * 10000), user, s.getClothe());
            myCarts.stream().filter(cart1 -> cart1.getCustomer().getId() == user.getId() && cart1.getClothe().getId() == cart.getClothe().getId())
                    .forEach(cart1 -> {
                        cart1.getClothe().setQuantity(cart1.getClothe().getQuantity() + cart.getClothe().getQuantity());
                        flag.set(true);
                    });
            if (flag.get() == false) myCarts.add(cart);
        });
        carts.removeAll(carts);
        while (true) {
            System.out.println("1=>Clothes List. 2=>BuyClothes. 3=>My Cart. 4=>Checkout. 5=>BuyingHistoryAction. 6=>AverageCost. 7=>Balance. 0=>Logout");
            try {
                int option = new Scanner(System.in).nextInt();
                switch (option) {
                    case 1:
                        clotheService.read();
                        break;
                    case 2:
                        addToCart(user);
                        break;
                    case 3:
                        cartList(user);
                        break;
                    case 4:
                        checkOut(user);
                        break;
                    case 5:
                        historyService.historyMenu(user);
                        break;
                    case 6:
                        averageCost(user);
                        break;
                    case 7:
                        System.out.println("Balance : " + user.getBalance());
                        break;
                    case 0:
                        LocalDateTime last = LocalDateTime.now();
                        user.setLast_online(last);
                        return;

                }
            } catch (InputMismatchException e) {
                System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                return;
            }
        }
    }

    @Override
    public void addToCart(User user) {
        clotheService.read();
        System.out.println("Add to Cart");
        System.out.println("Enter id");
        try {
            int clotheId = new Scanner(System.in).nextInt();
            Cloth selectedCloth = cloths.stream().filter(s -> s.getId() == clotheId).findFirst().orElse(null);
            if (selectedCloth != null) {
                System.out.println("Enter quantity");
                double quantity = new Scanner(System.in).nextDouble();
                if (selectedCloth.getQuantity() >= quantity) {
                    Cloth cartCloth = new Cloth(selectedCloth.getId(), selectedCloth.getName(), selectedCloth.getSize(), selectedCloth.getColor(), selectedCloth.getPrice(), quantity, selectedCloth.getDiscount());

                    if (user != null) {
                        AtomicBoolean flag = new AtomicBoolean(false);
                        Cart cart = new Cart((int) (Math.random() * 10000), user, cartCloth);
                        myCarts.stream().filter(cart1 -> cart1.getCustomer().getId() == user.getId() && cart1.getClothe().getId() == cart.getClothe().getId())
                                .forEach(cart1 -> {
                                    cart1.getClothe().setQuantity(cart1.getClothe().getQuantity() + cart.getClothe().getQuantity());
                                    flag.set(true);
                                });

                        if (flag.get() == false) myCarts.add(cart);
                    } else {
                        AtomicBoolean flag = new AtomicBoolean(false);
                        Cart cart = new Cart((int) (Math.random() * 10000), cartCloth);
                        carts.stream().filter(cart1 -> cart1.getClothe().getId() == cart.getClothe().getId())
                                .forEach(cart1 -> {
                                    cart1.getClothe().setQuantity(cart1.getClothe().getQuantity() + cart.getClothe().getQuantity());
                                    flag.set(true);
                                });

                        if (flag.get() == false) carts.add(cart);

                    }
                    System.out.println(TEXT_CYAN + SUCCESS + TEXT_CYAN);
                } else {
                    System.out.println(WRONG_OPTION);
                    return;
                }

            }
        } catch (InputMismatchException e) {
            System.out.println("Not found");
        }

    }

    @Override
    public void checkOut(User user) {
        if (user != null) buyClothes(user);
        System.out.println("1=>Login account 2=>Register");
        int option = new Scanner(System.in).nextInt();
        switch (option) {
            case 1:
                customerSerivce.login();
                break;
            case 2:
                customerSerivce.register();
                break;
        }

    }

    @Override
    public void cartList(User user) {
        if (user != null) {
            System.out.println("Your cart");
            AtomicReference<Double> total = new AtomicReference<>((double) 0);
            System.out.println(TEXT_CYAN + "----------------------------------------------------");
            myCarts.stream().filter(d -> d.getCustomer().getId() == user.getId()).forEach(s -> {
                System.out.println("name     : " + s.getClothe().getName() + "\nprice    : " + s.getClothe().getPrice() + "\nsize     : " + s.getClothe().getSize() + "\nquantity : " + s.getClothe().getQuantity() + "\ncolor    : " + s.getClothe().getColor());
                System.out.println("----------------------------------------------------" + TEXT_CYAN);
                total.updateAndGet(v -> new Double((double) (v + s.getClothe().getPrice() * s.getClothe().getQuantity())));
            });

            System.out.println(TEXT_GREEN + "Total   : " + total + TEXT_GREEN);
        } else {
            System.out.println("Your cart");
            AtomicReference<Double> total = new AtomicReference<>((double) 0);
            System.out.println(TEXT_CYAN + "----------------------------------------------------");
            carts.stream().forEach(s -> {
                System.out.println("name     : " + s.getClothe().getName() + "\nprice    : " + s.getClothe().getPrice() + "\nsize     : " + s.getClothe().getSize() + "\nquantity : " + s.getClothe().getQuantity());
                System.out.println("----------------------------------------------------" + TEXT_CYAN);
                total.updateAndGet(v -> new Double((double) (v + s.getClothe().getPrice() * s.getClothe().getQuantity())));
            });
            System.out.println(TEXT_GREEN + "Total   : " + total + TEXT_GREEN);
            System.out.println("Checkout=>1 Back=>0");
            try {
                int option = new Scanner(System.in).nextInt();
                switch (option) {
                    case 1:
                        checkOut(user);
                        break;
                    case 2:
                        return;
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_OPTION);
                return;
            }

        }
    }

    @Override
    public void buyClothes(User user) {

        while (true) {
            cartList(user);
            System.out.println(TEXT_RED + "Buy all =>1. Remove quantity=>2. Remove item=>3.  Back=>0" + TEXT_RED);
            try {
                int option = new Scanner(System.in).nextInt();
                switch (option) {
                    case 1:
                        paymentService.read();
                        System.out.println("Enter payType id");
                        try {
                            int payId = new Scanner(System.in).nextInt();
                            PayType selectPayType = findObject.returnPayType(payId);
                            try {
                                System.out.println(selectPayType.getName());

                                double total = 0;
                                double count = 0;

                                for (Cart myCart : myCarts) {
                                    if (myCart.getCustomer().getId() == user.getId()) {
                                        if (myCart.getClothe().getDiscount() > 0) {
                                            count += myCart.getClothe().getQuantity() * (myCart.getClothe().getPrice() + myCart.getClothe().getPrice() * myCart.getClothe().getDiscount() / 100) + (myCart.getClothe().getQuantity() * myCart.getClothe().getPrice() * selectPayType.getCommissonFee() / 100);
                                        } else {
                                            count += myCart.getClothe().getQuantity() * myCart.getClothe().getPrice() + (myCart.getClothe().getQuantity() * myCart.getClothe().getPrice() * selectPayType.getCommissonFee() / 100);
                                        }
                                    }
                                }
                                System.out.println("Total with commissonFee : " + count);

                                if (user.getBalance() >= count) {
                                    for (Cart myCart : myCarts) {
                                        if (myCart.getCustomer().getId() == user.getId()) {
                                            Cloth newCloth = myCart.getClothe();
                                            customerCloths.add(newCloth);
                                            total += myCart.getClothe().getQuantity() * myCart.getClothe().getPrice();
                                            for (Cloth cloth : cloths) {
                                                if (cloth.getName().equals(myCart.getClothe().getName()) && cloth.getColor().equals(myCart.getClothe().getColor()) && cloth.getSize().equals(myCart.getClothe().getSize())) {
                                                    cloth.setQuantity(cloth.getQuantity() - myCart.getClothe().getQuantity());
                                                    break;
                                                }
                                            }

                                        }
                                    }

                                    user.setBalance(user.getBalance() - (total + total * selectPayType.getCommissonFee() / 100));
                                    User user1 = users.stream().filter(s -> s.getRole().equals(CustomerRole.ADMIN)).findFirst().orElse(null);
                                    user1.setBalance(user1.getBalance() + (total + total * selectPayType.getCommissonFee() / 100));

                                    LocalDateTime created_at = LocalDateTime.now();

                                    PurchaseHistory purchaseHistory = new PurchaseHistory((int) (Math.random() * 10000), user, customerCloths, selectPayType, created_at);
                                    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
                                    String line = "----------------------\n";
                                    String userLine = "User : " + user.getName() + "\n";
                                    String payLine = "Pay  : " + selectPayType.getName() + "\n";
                                    String dateLine = "Date : " + created_at.format(format) + "\n";
                                    String clothes = "Clothes" + "\n";
                                    String endline = "----------------------\n";

                                    try {
                                        File targetFile = new File("history.txt");
                                        OutputStream outStream = new FileOutputStream(targetFile, true);
                                        outStream.write(line.getBytes());
                                        outStream.write(userLine.getBytes());
                                        outStream.write(payLine.getBytes());
                                        outStream.write(dateLine.getBytes());
                                        outStream.write(clothes.getBytes());

                                        for (Cart myCart : myCarts) {
                                            if (myCart.getCustomer().getId() == user.getId()) {
                                                for (Cloth cloth : cloths) {
                                                    if (cloth.getName().equals(myCart.getClothe().getName()) && cloth.getColor().equals(myCart.getClothe().getColor()) && cloth.getSize().equals(myCart.getClothe().getSize())) {
                                                        String clothLine = "id : " + myCart.getId() + ", name : " + myCart.getClothe().getName() + ", price : " + myCart.getClothe().getPrice() + ", quantity : " + myCart.getClothe().getQuantity() + ", color : " + myCart.getClothe().getColor() + ", size : " + myCart.getClothe().getSize() + "\n";
                                                        outStream.write(clothLine.getBytes());
                                                        break;
                                                    }
                                                }

                                            }
                                        }

                                        outStream.write(endline.getBytes());

                                    } catch (IOException e) {
                                        System.out.println(NOT_FOUND);
                                    }


                                    customerHistories.add(purchaseHistory);

                                    List<Cart> temp = new ArrayList<>();
                                    myCarts.stream().filter(s -> s.getCustomer().getId() == user.getId()).forEach(s -> temp.add(s));

                                    myCarts.removeAll(temp);

                                    customerCloths.removeAll(customerCloths);
                                    System.out.println(TEXT_CYAN + BUY + TEXT_CYAN);
                                } else {
                                    System.out.println(CHECK_BALANCE);
                                }
                            } catch (NullPointerException e) {
                                System.out.println(PAY_NOT_FOUND);
                            }
                        } catch (InputMismatchException e) {
                            System.out.println(WRONG_OPTION);
                        }
                        break;
                    case 2:
                        cartList(user);
                        System.out.println("Enter clothe id remove some qty");
                        try {
                            int clothId = new Scanner(System.in).nextInt();
                            Cloth cloth = findObject.returnClothe(clothId);
                            if (cloth != null) {
                                myCarts.stream().filter(s -> s.getClothe().getId() == cloth.getId()).forEach(s -> System.out.println("Cloth : " + s.getClothe().getName() + ", quantity : " + s.getClothe().getQuantity()));

                                System.out.println("Enter quantity");
                                try {
                                    double quantity = new Scanner(System.in).nextDouble();
                                    myCarts.stream().filter(s -> s.getClothe().getId() == cloth.getId() && s.getCustomer().getId() == user.getId()).forEach(s -> s.getClothe().setQuantity(s.getClothe().getQuantity() - quantity));
                                    System.out.println(SUCCESS);
                                } catch (InputMismatchException e) {
                                    System.out.println(WRONG_OPTION);
                                }
                            }
                        } catch (InputMismatchException e) {
                            System.out.println(WRONG_OPTION);
                        }
                        break;
                    case 3:
                        cartList(user);
                        System.out.println("Enter clothe id for remove");
                        int clotheId = new Scanner(System.in).nextInt();
                        Cart cart = myCarts.stream().filter(s -> s.getClothe().getId() == clotheId && s.getCustomer().getId() == user.getId()).findFirst().orElse(null);
                        myCarts.remove(cart);
                        System.out.println(TEXT_RED + REMOVED + TEXT_RED);
                        break;
                    case 0:
                        cartMenu(user);
                        break;

                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_OPTION);
            }
        }

    }


    private void averageCost(User user) {
        AtomicReference<Double> sum = new AtomicReference<>((double) 0);
        AtomicReference<Double> total = new AtomicReference<>((double) 0);
        customerHistories.stream().filter(purchaseHistory -> purchaseHistory.getUser().getId() == user.getId()).forEach(historyService -> {
            total.updateAndGet(v -> new Double((double) (v + historyService.getCloths().stream().mapToDouble(Cloth::getQuantity).sum())));
            sum.updateAndGet(v -> new Double((double) (v + historyService.getCloths().stream().mapToDouble(cloth -> cloth.getQuantity() * cloth.getPrice()).sum())));
        });

        double result = sum.get() / total.get();


        System.out.println(TEXT_CYAN + "--------------");
        System.out.println("Average total : " + result);
        System.out.println("--------------" + TEXT_CYAN);
    }

}

