package uz.pdp.services.auth;

import uz.pdp.model.user.User;
import uz.pdp.services.action.FindObject;
import uz.pdp.services.interfaces.user.AuthService;
import uz.pdp.services.user.admin.AdminService;
import uz.pdp.services.user.customer.CustomerServiceImpl;
import uz.pdp.services.user.customer.cart.CartServiceImpl;

import java.time.LocalDateTime;

import static uz.pdp.DataBase.*;
import static uz.pdp.helpers.MessageHelper.*;

public class AuthServiceImpl implements AuthService {

    static AdminService adminService = new AdminService();
    static CustomerServiceImpl customerServiceImpl = new CustomerServiceImpl();
    static FindObject findObject = new FindObject();
    static CartServiceImpl cartService = new CartServiceImpl();

    @Override
    public void login() {
        System.out.println("Enter username");
        String username = scannerStr.nextLine();
        System.out.println("Enter password");
        String password = scannerStr.nextLine();
        User userAuth = findObject.findUser(username, password);

        if (userAuth != null && userAuth.getLoginAttempt() < 3) {
            switch (userAuth.getRole()) {
                case ADMIN:
                    carts.removeAll(carts);
                    adminService.adminMenu(userAuth);
                    break;
                case CUSTOMER_ROLE:
                    cartService.cartMenu(userAuth);
                    break;
                default:
                    System.out.println(WRONG_OPTION);
                    break;
            }
        } else if (userAuth != null && userAuth.getLoginAttempt() >= 3) {
            carts.removeAll(carts);
            System.out.println(TEXT_RED + BLOCK_LIST + TEXT_RED);
        } else {
            System.out.println(NOT_FOUND);
        }

    }


    @Override
    public void register() {
        System.out.println("Enter username");
        String username = scannerStr.nextLine();
        System.out.println("Enter password");
        String password = scannerStr.nextLine();
        User userReg = users.stream().filter(customer -> customer.getName().equals(username) && customer.getPassword().equals(password)).findFirst().orElse(null);
        if (userReg != null) {
            System.out.println("User already exist");
            return;
        }
        LocalDateTime created_at = LocalDateTime.now();
        LocalDateTime updated_at = LocalDateTime.now();

        User user = new User((int) (Math.random() * 10000), username, password, created_at, updated_at);
        user.setLast_online(created_at);
        users.add(user);
        System.out.println(TEXT_CYAN + CREATED + TEXT_CYAN);
    }

    @Override
    public void customers(User user) {
        switch (user.getRole()) {
            case ADMIN:
                users.stream().forEach(s -> System.out.println(TEXT_GREEN + "name : " + s.getName() + ", role : " + s.getRole() + ", created : " + s.getDateFormat() + ", updated : " + s.getUpdateFormat() + TEXT_GREEN));
                break;
            case CUSTOMER_ROLE:
                users.stream().filter(s -> s.equals(user)).forEach(s -> System.out.println(TEXT_GREEN + "name : " + s.getName() + ", role : " + s.getRole() + ", created : " + s.getDateFormat() + ", updated : " + s.getUpdateFormat() + TEXT_GREEN));
                break;

        }
    }
}
