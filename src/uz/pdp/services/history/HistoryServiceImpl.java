package uz.pdp.services.history;

import uz.pdp.model.enums.ClotheColor;
import uz.pdp.model.enums.ClotheSize;
import uz.pdp.model.payment.PayType;
import uz.pdp.model.product.Cloth;
import uz.pdp.model.user.User;
import uz.pdp.services.action.FindObject;
import uz.pdp.services.user.admin.action.PaymentServiceImpl;
import uz.pdp.services.interfaces.history.HistoryService;

import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicReference;

import static uz.pdp.DataBase.*;
import static uz.pdp.helpers.MessageHelper.PAY_NOT_FOUND;
import static uz.pdp.helpers.MessageHelper.WRONG_OPTION;

public class HistoryServiceImpl implements HistoryService {

    static PaymentServiceImpl paymentService = new PaymentServiceImpl();
    static FindObject findObject = new FindObject();

    @Override
    public void customerHistory(User user) {
        switch (user.getRole()) {
            case ADMIN:
                customerHistories.stream().forEach(s -> {
                    System.out.println("User    : " + s.getUser().getName());
                    System.out.println("Payment : " + s.getPayType().getName());
                    System.out.println("Date    : " + s.getDateFormat());
                    System.out.println("Clothes");
                    s.getCloths().stream()
                            .forEach(c -> {
                                System.out.println("id : " + c.getId() + ", name : " + c.getName()
                                        + ", price : " + c.getPrice() + ", quantity : " + c.getQuantity() + ", size : " + c.getSize() + ", color : " + c.getColor());
                            });
                    System.out.println(TEXT_RED + "----------------------------------------------------" + TEXT_RED);
                });
                break;
            case CUSTOMER_ROLE:
                customerHistories.stream().filter(customerHistory -> customerHistory.getUser().getId() == user.getId())
                        .forEach(s -> {
                            System.out.println("User : " + s.getUser().getName());
                            System.out.println("Payment : " + s.getPayType().getName());
                            System.out.println("Date    : " + s.getDateFormat());
                            System.out.println("Clothes");
                            s.getCloths().stream()
                                    .forEach(c -> {
                                        System.out.println("id : " + c.getId() + ", name : " + c.getName()
                                                + ", price : " + c.getPrice() + ", quantity : " + c.getQuantity() + ", size : " + c.getSize());
                                    });

                            System.out.println(TEXT_RED + "----------------------------------------------------" + TEXT_RED);
                        });
                break;
        }
    }

    @Override
    public void clothSize(User user) {
        customerHistory(user);
        switch (user.getRole()) {
            case ADMIN: {
                System.out.println("Enter size");
                try {
                    String size = new Scanner(System.in).nextLine();
                    try {
                        ClotheSize clotheSize = ClotheSize.valueOf(size);
                        customerHistories.stream()
                                .forEach(
                                        s -> s.getCloths().stream()
                                                .filter(d -> d.getSize().equals(clotheSize))
                                                .forEach(e ->
                                                {
                                                    System.out.println("User " + s.getUser().getName());
                                                    System.out.println("id : " + e.getId() + ", name : " + e.getName() +
                                                            ", size : " + e.getSize() + ", price : " + e.getPrice() +
                                                            ", quantity : " + e.getQuantity());
                                                })
                                );

                    } catch (IllegalArgumentException e) {
                        System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                }
                break;
            }
            case CUSTOMER_ROLE: {
                System.out.println("Enter size");
                try {
                    String size = new Scanner(System.in).nextLine();
                    try {
                        ClotheSize clotheSize = ClotheSize.valueOf(size);
                        customerHistories.stream()
                                .filter(s -> s.getUser().getId() == user.getId())
                                .forEach(
                                        s -> s.getCloths().stream()
                                                .filter(d -> d.getSize().equals(clotheSize))
                                                .forEach(e ->
                                                {
                                                    System.out.println("id : " + e.getId() + ", name : " + e.getName() +
                                                            ", size : " + e.getSize() + ", price : " + e.getPrice() +
                                                            ", quantity : " + e.getQuantity());
                                                })
                                );

                    } catch (IllegalArgumentException e) {
                        System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                }
                break;
            }
        }
    }

    @Override
    public void clothName(User user) {
        customerHistory(user);
        switch (user.getRole()) {
            case ADMIN: {
                System.out.println("Enter name");
                try {
                    String name = new Scanner(System.in).nextLine();
                    Cloth cloth = findObject.returnCloth(name);
                    try {
                        System.out.println(cloth.getName());
                        customerHistories.stream()
                                .forEach(
                                        s -> s.getCloths().stream()
                                                .filter(d -> d.getName().equalsIgnoreCase(name))
                                                .forEach(e ->
                                                {
                                                    System.out.println("User " + s.getUser().getName());
                                                    System.out.println("id : " + e.getId() + ", name : " + e.getName() +
                                                            ", price : " + e.getPrice() + " , color : " + e.getColor() +
                                                            ", size : " + e.getSize() + " ,quantity : " + e.getQuantity());
                                                })
                                );
                    } catch (NullPointerException e) {
                        System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                }
                break;
            }
            case CUSTOMER_ROLE: {
                System.out.println("Enter name");
                try {
                    String name = new Scanner(System.in).nextLine();
                    Cloth cloth = findObject.returnCloth(name);
                    try {
                        System.out.println(cloth.getName());
                        customerHistories.stream()
                                .filter(s -> s.getUser().getId() == user.getId())
                                .forEach(
                                        s -> s.getCloths().stream()
                                                .filter(d -> d.getName().equalsIgnoreCase(name))
                                                .forEach(e ->
                                                {
                                                    System.out.println("id : " + e.getId() + ", name : " + e.getName() +
                                                            ", price : " + e.getPrice() + " , color : " + e.getColor() +
                                                            ", size : " + e.getSize() + " ,quantity : " + e.getQuantity());
                                                })
                                );
                    } catch (NullPointerException e) {
                        System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                }
                break;
            }
        }

    }

    @Override
    public void clothColor(User user) {
        customerHistory(user);
        switch (user.getRole()) {
            case ADMIN: {
                System.out.println("Enter color");
                try {
                    String color = new Scanner(System.in).nextLine();
                    try {
                        ClotheColor clotheColor = ClotheColor.valueOf(color);
                        customerHistories.stream()
                                .forEach(
                                        s -> s.getCloths().stream()
                                                .filter(d -> d.getColor().equals(clotheColor))
                                                .forEach(e ->
                                                {
                                                    System.out.println("User " + s.getUser().getName());
                                                    System.out.println("id : " + e.getId() + ", name : " + e.getName() +
                                                            ", size : " + e.getSize() + ", price : " + e.getPrice() +
                                                            ", quantity : " + e.getQuantity());
                                                })
                                );

                    } catch (IllegalArgumentException e) {
                        System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                }
                break;
            }
            case CUSTOMER_ROLE: {
                System.out.println("Enter color");
                try {
                    String color = new Scanner(System.in).nextLine();
                    try {
                        ClotheColor clotheColor = ClotheColor.valueOf(color);
                        customerHistories.stream()
                                .filter(s -> s.getUser().getId() == user.getId())
                                .forEach(
                                        s -> s.getCloths().stream()
                                                .filter(d -> d.getColor().equals(clotheColor))
                                                .forEach(e ->
                                                {
                                                    System.out.println("id : " + e.getId() + ", name : " + e.getName() +
                                                            ", size : " + e.getSize() + ", price : " + e.getPrice() +
                                                            ", quantity : " + e.getQuantity());
                                                })
                                );

                    } catch (IllegalArgumentException e) {
                        System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                    }
                } catch (InputMismatchException e) {
                    System.out.println(TEXT_RED + WRONG_OPTION + TEXT_RED);
                }
                break;
            }
        }

    }


    @Override
    public void payType(User user) {
        paymentService.read();
        switch (user.getRole()) {
            case ADMIN: {
                System.out.println("Enter paytype id");
                try {
                    int payTypeId = new Scanner(System.in).nextInt();
                    PayType payType = payTypes.stream()
                            .filter(s -> s.getId() == payTypeId)
                            .findFirst().orElse(null);
                    try {
                        System.out.println(TEXT_BLUE + "--" + payType.getName() + "--" + TEXT_BLUE);
                        AtomicReference<Double> total = new AtomicReference<>((double) 0);
                        customerHistories.stream()
                                .filter(s -> s.getPayType().equals(payType))
                                .forEach(s -> {
                                    System.out.println("Date    : " + s.getDateFormat());
                                    System.out.println("Clothes : ");
                                    s.getCloths().stream()
                                            .forEach(d -> {
                                                System.out.println("User " + s.getUser().getName());
                                                System.out.println("name : " + d.getName() + ", quantity : " + d.getQuantity() + ", size : " + d.getSize() + ", price : " + d.getSize());
                                                total.updateAndGet(v -> new Double((double) (v + d.getQuantity() + d.getPrice())));
                                            });
                                });
                        System.out.println(TEXT_CYAN + "Total : " + total + TEXT_CYAN);

                    } catch (NullPointerException e) {
                        System.out.println(TEXT_RED + PAY_NOT_FOUND + TEXT_RED);
                        return;
                    }
                } catch (
                        InputMismatchException e) {
                    System.out.println(TEXT_RED + PAY_NOT_FOUND + TEXT_RED);
                }
                break;
            }
            case CUSTOMER_ROLE: {
                System.out.println("Enter paytype id");
                try {
                    int payTypeId = new Scanner(System.in).nextInt();
                    PayType payType = payTypes.stream()
                            .filter(s -> s.getId() == payTypeId)
                            .findFirst().orElse(null);
                    try {
                        System.out.println(TEXT_BLUE + "--" + payType.getName() + "--" + TEXT_BLUE);
                        AtomicReference<Double> total = new AtomicReference<>((double) 0);
                        customerHistories.stream()
                                .filter(s -> s.getPayType().equals(payType) && s.getUser().getId() == user.getId())
                                .forEach(s -> {
                                    System.out.println("Date    : " + s.getDateFormat());
                                    System.out.println("Clothes : ");
                                    s.getCloths().stream()
                                            .forEach(d -> {
                                                System.out.println("name : " + d.getName() + ", quantity : " + d.getQuantity() + ", size : " + d.getSize() + ", price : " + d.getSize());
                                                total.updateAndGet(v -> new Double((double) (v + d.getQuantity() + d.getPrice())));
                                            });
                                });
                        System.out.println(TEXT_CYAN + "Total : " + total + TEXT_CYAN);

                    } catch (NullPointerException e) {
                        System.out.println(TEXT_RED + PAY_NOT_FOUND + TEXT_RED);
                        return;
                    }
                } catch (
                        InputMismatchException e) {
                    System.out.println(TEXT_RED + PAY_NOT_FOUND + TEXT_RED);
                }
                break;
            }
        }
    }


    @Override
    public void historyMenu(User user) {
        while (true) {
            System.out.println("1=>History List. 2=>History by PayType. 3=>History ClothSize. 4=>History ClothColor. 5=>HistoryClothName 0=>Back");
            try {
                int optionPay = new Scanner(System.in).nextInt();
                switch (optionPay) {
                    case 1:
                        customerHistory(user);
                        break;
                    case 2:
                        payType(user);
                        break;
                    case 3:
                        clothSize(user);
                        break;
                    case 4:
                        clothColor(user);
                        break;
                    case 5:
                        clothName(user);
                    case 0:
                        return;
                    default:
                        System.out.println(WRONG_OPTION);
                }
            } catch (InputMismatchException e) {
                System.out.println(WRONG_OPTION);
            }
        }
    }
}
