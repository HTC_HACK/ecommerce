package uz.pdp.services.action;

import uz.pdp.model.payment.PayType;
import uz.pdp.model.product.Cloth;
import uz.pdp.model.user.User;

import static uz.pdp.DataBase.*;

public class FindObject {

    public Cloth returnClothe(int id) {
        return cloths.stream().filter(s -> s.getId() == id).findFirst().orElse(null);
    }

    public Cloth returnCloth(String name) {
        return cloths.stream()
                .filter(s -> s.getName().equalsIgnoreCase(name))
                .findFirst()
                .orElse(null);
    }

    public PayType returnPayType(String name) {
        return payTypes.stream().filter(s -> s.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
    }

    public PayType returnPayType(int id) {
        return payTypes.stream().filter(s -> s.getId() == id).findFirst().orElse(null);
    }

    public void clotheList(Cloth cloth) {
        cloths.stream()
                .filter(s -> s.getName().equals(cloth.getName()))
                .forEach(s -> System.out.println("name : " + s.getName()));
    }

    public User findUser(String username, String password) {

//        users.stream()
//                .filter(customer -> customer.getName().equals(username))
//                .filter(customer -> customer.getPassword().equals(password))
//                .findFirst().orElse(null);

        for (User user : users) {
            if (user.getName().equals(username)) {
                if (user.getLoginAttempt() >= 3)
                    return user;
                if (user.getPassword().equals(password))
                    return user;
                user.setLoginAttempt(user.getLoginAttempt() + 1);
            }
        }
        return null;
    }

}
