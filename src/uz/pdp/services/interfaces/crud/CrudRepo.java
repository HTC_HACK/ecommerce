package uz.pdp.services.interfaces.crud;

public interface CrudRepo {

    void create();

    void read();

    void update();

    void delete();

    void crudMenu();

    String returnName();

}
