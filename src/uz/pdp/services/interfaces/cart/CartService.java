package uz.pdp.services.interfaces.cart;

import uz.pdp.model.user.User;

public interface CartService {

    void cartMenu(User user);

    void addToCart(User user);

    void checkOut(User user);

    void cartList(User user);

    void buyClothes(User user);

}
