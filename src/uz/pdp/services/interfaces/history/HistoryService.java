package uz.pdp.services.interfaces.history;

import uz.pdp.model.user.User;

public interface HistoryService {

    void customerHistory(User user);

    void clothSize(User user);

    void clothName(User user);

    void clothColor(User user);

    void payType(User user);

    void historyMenu(User user);

}
