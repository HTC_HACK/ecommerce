package uz.pdp.services.interfaces.builder;

import uz.pdp.model.product.Cloth;
import uz.pdp.model.enums.ClotheColor;
import uz.pdp.model.enums.ClotheSize;

@FunctionalInterface
public interface ClotheBuilder {

    Cloth build(Integer id, String name,
                ClotheSize size, ClotheColor color,
                double price, double quantity,
                double discount);

}
