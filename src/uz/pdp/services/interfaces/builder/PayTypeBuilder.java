package uz.pdp.services.interfaces.builder;

import uz.pdp.model.payment.PayType;

public interface PayTypeBuilder {

    PayType build(Integer id, String name, double commissonFee);

}
