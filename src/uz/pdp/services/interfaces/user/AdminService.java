package uz.pdp.services.interfaces.user;

import uz.pdp.model.user.User;

public interface AdminService {

    void adminMenu(User user);

}
