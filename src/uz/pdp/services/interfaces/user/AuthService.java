package uz.pdp.services.interfaces.user;

import uz.pdp.model.user.User;

public interface AuthService {

    void login();

    void register();

    void customers(User user);


}
