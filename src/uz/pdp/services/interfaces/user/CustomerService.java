package uz.pdp.services.interfaces.user;

import uz.pdp.model.user.User;

public interface CustomerService {

    void customerMenu(User user);

}
