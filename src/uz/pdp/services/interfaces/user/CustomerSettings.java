package uz.pdp.services.interfaces.user;

public interface CustomerSettings {

    void list();

    void blockList();

    void unblock();

    void settingsMenu();

}
